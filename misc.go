package se3f03base 

import (
	"github.com/kr/pty"
	
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"sort"
	"strings"
	"sync"
	"time"
)

// Search the list for both the name, and name.sh.  If both exist, one will be returned.  If neither exist, an empty string is returned.
func FindScript(script string, files []string) string {
	for _, f := range files {
		if strings.HasPrefix(f, script) {
			return f
		}
	}
	return ""
}

func ExecuteScript(args ...string) string {
	cmd := exec.Command("bash", args...)
	
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Bad cmd execution: ", err)
	}
	
	return string(out)
}

func ExecuteScriptWithStdin(args ...string) string {
	cmd := exec.Command("bash", args...)
	
	pty, tty, err := pty.Open()
	if err != nil {
		fmt.Println("Failed to open pty!!!!", err)
		panic(err)
	}
	defer pty.Close()
	defer tty.Close()
	cmd.Stdin = tty
	
	B := &bytes.Buffer{}
	newOut := io.MultiWriter(B, os.Stdout)
	
	cmd.Stdout = newOut
	cmd.Stderr = newOut
	
	//Write to newOut, so input is captured!
	go func() {
		_, err := io.Copy(pty, io.TeeReader(os.Stdin, newOut))
		fmt.Println(err)
	}()
	
	err = cmd.Run()
	if err != nil {
		fmt.Println("Bad cmd execution: ", err)
	}
	
	return string(B.Bytes())
}

func CompareNewlineStrings(s1, s2 string) bool {
	sl1 := strings.Split(s1, "\n")
	sl2 := strings.Split(s2, "\n")
	
	if len(sl1) != len(sl2) {
		return false
	}
	
	sort.Strings(sl1)
	sort.Strings(sl2)
	
	for i := range sl1 {
		if sl1[i] != sl2[i] {
			return false
		}
	}
	
	return true
}


var Grade int
var GM sync.Mutex

func init() {
	go func() {
		<- time.After(30*time.Second)
		GM.Lock()
		fmt.Println("Time out running code, returning current grade of: ", Grade)
		os.Exit(Grade)
	}()
}
