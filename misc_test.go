package se3f03base

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestFindScript(t *testing.T) {
	files := []string{
		"noit",
		"noext",
		"ext.sh",
		"ext2.bash",
		"fnameCapSmall",
		"both",
		"both.sh",
		"both.bash",
	}
	Convey("FindScript return \"\" on a non-existant file", t, func() {
		So(FindScript("nothere", files), ShouldEqual, "")
	})
	Convey("FindScript finds the file in the list when", t, func() {
		Convey("There is no extra extension", func() {
			So(FindScript("noext", files), ShouldEqual, "noext")
			So(FindScript("fnameCapSmall", files), ShouldEqual, "fnameCapSmall")
		})
		Convey("When there is an extension", func() {
			So(FindScript("ext", files), ShouldEqual, "ext.sh")
			So(FindScript("ext2", files), ShouldEqual, "ext2.bash")
		})
		Convey("When there is both files existing", func() {
			So(FindScript("both", files), ShouldEqual, "both")
		})
	})
}

func TestNewlineStrings(t *testing.T) {
	Convey("Should find same one liners the same", t, func() {
		So(CompareNewlineStrings("line1", "line1"), ShouldBeTrue)
	})
	Convey("Should find different one liners different", t, func() {
		So(CompareNewlineStrings("line1", "linenot1"), ShouldBeFalse)
	})
	Convey("Should find different lengths different", t, func() {
		So(CompareNewlineStrings("line1\nline2", "line1"), ShouldBeFalse)
	})
	Convey("Should find multi liners the same", t, func() {
		Convey("While sorted the same", func() {
			So(CompareNewlineStrings("line1\nline2\nline3", "line1\nline2\nline3"), ShouldBeTrue)
		})
		Convey("While sorted differently", func() {
			So(CompareNewlineStrings("line1\nline2\nline3", "line2\nline3\nline1"), ShouldBeTrue)
		})
	})
	Convey("Should find multi liners different", t, func() {
		Convey("While sorted the same", func() {
			So(CompareNewlineStrings("line1\nline2\nline3", "line1\nline2\nline4"), ShouldBeFalse)
		})
		Convey("While sorted differently", func() {
			So(CompareNewlineStrings("line1\nline2\nline3", "line1\nline4\nline2"), ShouldBeFalse)
		})
	})
}
