FROM mjdsys/ubuntu-saucy-i386
MAINTAINER Matthew Dawson, matthew@mjdsystems.ca

RUN apt-get update
RUN apt-get -y dist-upgrade

RUN apt-get install -y build-essential nasm golang
